//
//  ScoreController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

class SidebarViewController: UITableViewController
{
    
    let menuItems = ["Title", "DayData", "WeekReport", "SportsInfo", "Advice"]
    var heightCache: Dictionary<String, CGFloat> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Utility.UIColorFromHex(SidebarBackgroundColor)
        tableView.backgroundColor = Utility.UIColorFromHex(SidebarBackgroundColor)
        
        
    }
    
    override func tableView(tableView: UITableView!, willDisplayCell cell: UITableViewCell!, forRowAtIndexPath indexPath: NSIndexPath!) {
        cell.backgroundColor = Utility.UIColorFromHex(SidebarBackgroundColor)
    }
    
//
//    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
//        var cell = tableView.dequeueReusableCellWithIdentifier(menuItems[indexPath.row], forIndexPath: indexPath) as UITableViewCell
//        
//        cell.backgroundColor = Utility.UIColorFromHex(SidebarBackgroundColor)
//        
//        return cell
//    }
//
//    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
//        return menuItems.count
//    }
//
//    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
//
//        var cellIdentifier = menuItems[indexPath.row]
//        
//        var cachedHeight = heightCache[cellIdentifier]
//        
//        if cachedHeight {
//            return cachedHeight!
//        }
//        
//        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell
//        heightCache[cellIdentifier] = cell.bounds.size.height
//        
//        println(cell.bounds.size.height)
//        
//        return heightCache[cellIdentifier]!
//
//    }
    
}
