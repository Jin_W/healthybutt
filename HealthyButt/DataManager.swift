//
//  DataManager.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import Foundation
import UIKit

var messages: [Message] = []
var seeds: [Message] = [Message.initWithIcon(UIImage(named: "WechatAvanta"), name: "郭逗逗", metaIcon: "Wechat", metaText: nil, body: "班长，我又变胖了。@HealthyButt"),
    Message.initWithIcon(UIImage(named: "Weibo"), name: nil, metaIcon: nil, metaText: "分享自 HealthyButt", body: "人生偶尔犯错，所以我要一坐再坐")]

class DataManager
{
    class func addMessage(message: Message) -> NSInteger {
        messages.append(message)
        return messages.count - 1
    }
    
    class func getMessageAtIndex(index: NSInteger) -> Message {
        
        if index < messages.count {
            return messages[index]
        }
        else {
            return generateMessage()
        }
    }
    
    class func generateMessage() -> Message {
        return seeds[random() % seeds.count]
    }
}