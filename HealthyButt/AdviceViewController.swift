//
//  ExerciseController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

let rightWhiteArrow = UIImage(named: "WhitePic")
let leftGreenArrow = UIImage(named: "GreenPic")

class AdviceViewController: UIViewController
{
    @IBOutlet var scrollView: UIScrollView
    @IBOutlet var nextBtn: UIImageView
    @IBOutlet var prevBtn: UIImageView
    @IBOutlet var advice: UILabel
    @IBOutlet var butt: UIImageView
    
    var timeDots: Array<UIImageView> = []
    var center: CGPoint!
    var currentPage = 0
    var totalPage = AdviceButtGuyFiles[currentIndex].count
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDots()
        addAdvicesImages()
        
        nextBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "nextBtnClicked"))
        prevBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "prevBtnClicked"))
        
        updateButtons()
        
        nextBtn.contentMode = UIViewContentMode.ScaleAspectFit
        nextBtn.userInteractionEnabled = true
        
        prevBtn.contentMode = UIViewContentMode.ScaleAspectFit
        prevBtn.userInteractionEnabled = true
        
        view.backgroundColor = backgroundColor
        
        butt.image = UIImage(named: pageButtFiles[currentIndex])
        butt.contentMode = UIViewContentMode.ScaleAspectFit
        butt.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "buttTapped"))
        butt.userInteractionEnabled = true
        
        advice.textAlignment = NSTextAlignment.Center
        advice.font = UIFont(name: chineseFont, size: 25)
        advice.textColor = UIColor.whiteColor()
        advice.numberOfLines = 3
        
        
        
    }
    
    func addAdvicesImages() {
        for i in 0..<totalPage {
            var imageView = UIImageView()
            
            imageView.frame = CGRectMake(CGFloat(Float(i) * Float(scrollView.frame.size.width)), 0.0, CGFloat(scrollView.frame.size.width), CGFloat(sqrt(2.5) * Float(radius)))
            
//            imageView.image = UIImage(named: AdviceButtGuyFiles[currentIndex][i])
            
            imageView.animationImages = [UIImage(named: SportsGuyFiles[2 * i]), UIImage(named: SportsGuyFiles[2 * i + 1])]
            imageView.animationDuration = 1.0
            
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            
            // Note, here must use bounds instead of frame.
            imageView.center.y = scrollView.bounds.midY
            
//            imageView.backgroundColor = UIColor.whiteColor()
            
            scrollView.addSubview(imageView)
            
            imageView.startAnimating()
        }
    }
    
    func updateButtons() {
        if totalPage == 1 || totalPage == 0 {
            nextBtn.image = UIImage()
            prevBtn.image = UIImage()
        }
        else {
            if currentPage == totalPage - 1 {
                nextBtn.image = UIImage(CGImage: leftGreenArrow.CGImage, scale: leftGreenArrow.scale, orientation: UIImageOrientation.UpMirrored)
            }
            else {
                nextBtn.image = rightWhiteArrow
            }
            if currentPage == 0 {
                prevBtn.image = leftGreenArrow
            }
            else {
                prevBtn.image = UIImage(CGImage: rightWhiteArrow.CGImage, scale: rightWhiteArrow.scale, orientation: UIImageOrientation.UpMirrored)
            }
        }
        
        advice.text = AdviceTitle[0][currentPage] + "\n" + AdviceBody[0][currentPage]
    }
    
    func nextBtnClicked() {
        
        if currentPage < AdviceButtGuyFiles[currentIndex].count - 1 {
            currentPage += 1
            scrollView.setContentOffset(CGPointMake((CGFloat)(currentPage) * scrollView.frame.size.width, 0), animated: true)
            updateButtons()
            
        }
    }
    
    func prevBtnClicked() {
        
        if currentPage > 0 {
            currentPage -= 1
            scrollView.setContentOffset(CGPointMake((CGFloat)(currentPage) * scrollView.frame.size.width, 0), animated: true)
            updateButtons()
        }
    }
    
    func buttTapped() {
        
        var displayController = parentViewController as DisplayController
        
        displayController.dismissAdviceViewController(self)
    }
    
    func setupDots() {
        prepareLayout()
        for i in 0..<dotNumber {
            timeDots.append(UIImageView(image: UIImage(named: "GreenDot")))
            layoutDot(i)
            view.addSubview(timeDots[i])
        }
    }
    
    func prepareLayout() {
        center = CGPointMake(scrollView.frame.midX, scrollView.frame.midY)
    }
    
    func layoutDot(index: NSInteger) {
        var angle: Float = Float(2.0) * Float(M_PI) * Float(index) / Float(dotNumber)
        var x = Float(center.x) + Float(radius) * sin(angle)
        var y = Float(center.y) - Float(radius) * cos(angle)
        
        timeDots[index].frame.size = CGSizeMake(CGFloat(dotSize), CGFloat(dotSize))
        timeDots[index].center = CGPointMake(CGFloat(x), CGFloat(y))
    }
    
}
