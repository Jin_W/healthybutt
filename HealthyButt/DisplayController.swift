//
//  DisplayController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

var communicationController: CommunicationController!

/**
 * This is the main page of the app. Will handle the interactions between other 
 * controllers. Meanwhile, it will be responsible for mantaining the view (layout).
 */
class DisplayController: UIViewController {
    
    override func viewDidLoad() {
        setNavigationBar()
        
        view.backgroundColor = backgroundColor
        
        
        let messageController = MessageController()
        
        for aController in childViewControllers {
            if let timer = aController as? TimerController {
                timer.timerControllerDelegate = messageController
            }
        }
        
        if communicationFlag == false {
            if !communicationController {
                communicationController = storyboard.instantiateViewControllerWithIdentifier("CommunicationController") as CommunicationController
            }
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("connectionStateChanged"), name: BluetoothConnectionStateChanged, object: nil)
//            presentViewController(communicationController, animated: true, completion: nil)
            
            NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("presentCommunicationController"), userInfo: nil, repeats: false)
            
        }
        
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    func connectionStateChanged() {
        if communicationFlag == false {
            var alertView = UIAlertView(title: nil, message: "Bluetooth disconnected.", delegate: self, cancelButtonTitle: "OK")
            alertView.show()
            
            presentCommunicationController()
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    func presentCommunicationController() {
        var nvController = UINavigationController(rootViewController: communicationController)
        
        presentViewController(nvController, animated: true, completion: nil)
    }
    
    func setNavigationBar() {
        
        // set left side sidebar menu icon
        let sidebarImage = UIImage(named: "MenuIcon")
        let sidebarButton = UIButton()
        sidebarButton.setImage(sidebarImage, forState: UIControlState.Normal)
        sidebarButton.frame = CGRectMake(0, 0, 22, 22)
        sidebarButton.imageView.contentMode = UIViewContentMode.ScaleAspectFit
        sidebarButton.addTarget(revealViewController(), action: "revealToggle:", forControlEvents: UIControlEvents.TouchUpInside)
//        var sidebarBarButton = UIBarButtonItem(image: Utility.scaleImage(sidebarImage, toSize: NaviBarIconSize), style: UIBarButtonItemStyle.Bordered, target: self, action: "sidebarButtonPressed:")
        let sidebarBarButton = UIBarButtonItem(customView: sidebarButton)
        navigationItem.leftBarButtonItem = sidebarBarButton
        
        // Set the side bar button action. When it's tapped, it'll show up the sidebar.
//        sidebarBarButton.target = revealViewController()
//        sidebarBarButton.action = Selector("revealToggle:")
        
        // set right side setting icon
        let settingImage = UIImage(named: "SetIcon")
        let settingButton = UIButton()
        settingButton.setImage(settingImage, forState: UIControlState.Normal)
        settingButton.frame = CGRectMake(0, 0, 22, 22)
        settingButton.imageView.contentMode = UIViewContentMode.ScaleAspectFit
        settingButton.addTarget(self, action: "settingButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
//        let settingBarButton = UIBarButtonItem(image: Utility.scaleImage(settingImage, toSize: NaviBarIconSize),
//            style: UIBarButtonItemStyle.Bordered, target: self,
//            action: "settingButtonPressed:")
        
        let settingBarButton = UIBarButtonItem(customView: settingButton)
        navigationItem.rightBarButtonItem = settingBarButton
        
        // set navigation bar transparent
        navigationController.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.translucent = true
//        navigationController.view.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
    }
    
    func settingButtonPressed(sender: AnyObject) {
        var settingController = self.storyboard.instantiateViewControllerWithIdentifier("SettingController") as SettingController
        self.navigationController.pushViewController(settingController, animated: true)
    }
    
    func sidebarButtonPressed(sender: AnyObject) {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // set the title
        navigationController.navigationBar.tintColor = UIColor.whiteColor()
        navigationController.navigationBar.titleTextAttributes = NSDictionary(objects: [titleFont, titleColor], forKeys: [NSFontAttributeName, NSForegroundColorAttributeName])
        
    }
    
    func presentAdviceViewController() {
        var adviceViewController = storyboard.instantiateViewControllerWithIdentifier("AdviceViewController") as AdviceViewController
        
        adviceViewController.view.frame = CGRectMake(0, view.frame.height, view.frame.width, view.frame.height)

        
        UIView.transitionWithView(view, duration: 0.5, options: nil, animations: {
            self.addChildViewController(adviceViewController)
            self.view.addSubview(adviceViewController.view)
            adviceViewController.view.frame = CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height)
            }, completion: nil)
        
    }
    
    func dismissAdviceViewController(child: AdviceViewController) {
        
        UIView.transitionWithView(view, duration: 0.5, options: nil, animations: {
            child.view.frame = CGRectMake(0.0, self.view.frame.height, self.view.frame.width, self.view.frame.height)
            }, completion: {
                (finished: Bool) -> Void in
                child.removeFromParentViewController()
                for aController in self.childViewControllers {
                    if let timer = aController as? TimerController {
                        timer.restart()
                    }
                }
            })
        
    }
    
}
