//
//  WelcomeViewController.swift
//  HealthyButt
//
//  Created by Jin Wang on 9/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

var currentIndex: NSInteger = 0

class WelcomeViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    var pageViewController: UIPageViewController!

    
    @IBOutlet var pageControl: UIPageControl
    @IBOutlet var iHaveButton: UIImageView
    @IBOutlet var iDontHaveButton: UIImageView
//    @IBOutlet var nextPageBtn: UIButton
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create page view controller
        pageViewController = storyboard.instantiateViewControllerWithIdentifier("IntroductionPageViewController") as UIPageViewController
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        iHaveButton.image = UIImage(named: "IhaveButt")
        iDontHaveButton.image = UIImage(named: "IhavenoButt")
        
        
        var startingViewController = viewControllerAtIndex(0) as IntroductionPageContentController!
        
        
        pageViewController.setViewControllers([startingViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
    
        pageViewController.view.backgroundColor = backgroundColor
        
        addChildViewController(pageViewController)
        
        view.addSubview(pageViewController.view)
        
        view.bringSubviewToFront(iHaveButton)
        view.bringSubviewToFront(iDontHaveButton)
        view.bringSubviewToFront(pageControl)
//        view.bringSubviewToFront(nextPageBtn)
        
        
//        nextPageBtn.addTarget(self, action: "nextPageBtnClicked", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        var buttonTapped = UITapGestureRecognizer(target: self, action: "iHaveButtonClicked")
        iHaveButton.userInteractionEnabled = true
        iHaveButton.addGestureRecognizer(buttonTapped)
        
        setPageControl()
        
    }
    
    
    func setPageControl() {
        pageControl.numberOfPages = pageTitles.count
    }
    
    func pageViewController(pageViewController: UIPageViewController!, viewControllerBeforeViewController viewController: UIViewController!) -> UIViewController! {
        
        var pageContentController = viewController as IntroductionPageContentController
        var index = pageContentController.pageIndex as Int
        
        if index == 0 || index == NSNotFound {
            return nil
        }
        
        index -= 1
        
        return viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController!, viewControllerAfterViewController viewController: UIViewController!) -> UIViewController! {
        
        var pageContentController = viewController as IntroductionPageContentController
        var index = pageContentController.pageIndex as NSInteger
        
        if index == NSNotFound {
            return nil
        }
        
        index += 1
        
        if index == pageTitles.count {
            return nil;
        }
        
        return viewControllerAtIndex(index)
    }
    
    func viewControllerAtIndex(index: NSInteger) -> IntroductionPageContentController? {
        
        if pageTitles.count == 0 || index >= pageTitles.count {
            return nil
        }

        // Create a new view controller and pass suitable data.
        var pageContentViewController = storyboard.instantiateViewControllerWithIdentifier("IntroductionPageContentController") as IntroductionPageContentController
        pageContentViewController.titleName = pageTitles[index]
        pageContentViewController.buttGuyFile = pageButtGuyFiles[index]
        pageContentViewController.buttFile = pageButtFiles[index]
        pageContentViewController.pageIndex = index
        
        return pageContentViewController

    }
    
    func pageViewController(pageViewController: UIPageViewController!, didFinishAnimating finished: Bool, previousViewControllers: [AnyObject]!, transitionCompleted completed: Bool) {
        var currentViewController = pageViewController.viewControllers[0] as IntroductionPageContentController
        pageControl.currentPage = currentViewController.pageIndex
    }
    
    func iHaveButtonClicked() {
//        var displayController = storyboard.instantiateViewControllerWithIdentifier("DisplayController") as DisplayController
//        currentIndex = pageControl.currentPage
//        
        var sidebarController = storyboard.instantiateViewControllerWithIdentifier("SidebarViewController") as SidebarViewController
//
        var navigationController = storyboard.instantiateViewControllerWithIdentifier("NavigationController") as UINavigationController
//
        var revealViewController = SWRevealViewController(rearViewController: sidebarController, frontViewController: navigationController)
//
////        window.rootViewController = revealViewController
//        
//        presentViewController(revealViewController, animated: true, completion: nil)
        
        
        /* Comment the following line if want to test without communicatrionController */
        currentIndex = pageControl.currentPage
        
//        communicationController = storyboard.instantiateViewControllerWithIdentifier("CommunicationController") as CommunicationController
        
       
        
        presentViewController(revealViewController, animated: true, completion: nil)
        
//        navigationController.pushViewController(revealViewController, animated: true)
    }

    
    
    
    
//    func presentationCountForPageViewController(pageViewController: UIPageViewController!) -> Int {
//        return pageTitles.count
//    }
//    
//    func presentationIndexForPageViewController(pageViewController: UIPageViewController!) -> Int {
//        return 0
//    }
//    
//    override func viewWillAppear(animated: Bool) {
////        navigationController.navigationBarHidden = true
//    }
//    
//    override func viewWillDisappear(animated: Bool) {
////        navigationController.navigationBarHidden = false
//    }
    
    
}
