//
//  Constants.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

// Common settings
let backgroundColor = Utility.getUIColorWithoutAlpha(88, green: 214, blue: 127)
let titleColor = UIColor.whiteColor()
let titleSize: CGFloat = 21
let iconSize: CGFloat = 18
let englishFont = "ArialRoundedMTBold"
let chineseFont = "RTWSShangGoG0v1-Regular"
let titleFont = UIFont(name: englishFont, size: titleSize)

let NaviBarIconSize = CGSizeMake(22, 22)

let TimerCounterNotification = "TimerCounterNotification"
let BluetoothConnectionStateChanged = "BluetoothConnectionStateChanged"

// Timer appearence settings
let dotNumber = 60
let timerLabelSize: Float = 46.5
let dotSize: Float = 10.0
let radius: Float = 100.0
let timerHeight = 300.0
var overTime = 60 * 60  // greater than 60 * 60
let reminderPoints = [10 * 3600 / dotNumber, 25 * 3600 / dotNumber] // should be sorted, the interval after overTime
//let messageInterval = 15 * 60
let timerSpeed = 120.0
let reminderTime = 5 // Support Minutes now

let dataCollectionTimeInterval = 5.0

// Introduction Page
let introductionPageTitleSize = 36
let pageTitles = ["我是寿司屁", "我是正脑屁", "我是别闹屁", "我是哟喂屁"]
let pageButtSportGuyFiles = ["YellowButtSportsGuy", "YellowButtSportsGuy", "YellowButtSportsGuy", "YellowButtSportsGuy"]
let pageButtGuyFiles = ["ShouSiJun", "BlackButtGuy", "GreenButtGuy", "YellowButtGuy"]
let pageButtFiles = ["ShouSiButt", "BlackButt", "GreenButt", "YellowButt"]

// Message Dialog
let MessageBodyTextSize: Float = 18.0
let MessageMetaTextSize: Float = 12.0
let MessageButtonTextSize: Float = 24.0
let MessageTitleSize: Float = 12.0
let MessageCompleteTextSize: Float = 24.0
let MessageCancelTime = 60

let ReminderBodyTextSize: Float = 15.0

// Sidebar
let SidebarBackgroundColor = 0x282828

// Advice Page
let AdviceButtGuyFiles: [[String]] = [["SportsGuy", "GreenButtGuy", "BlackButtGuy"], ["YellowButtGuy", "YellowButtGuy", "YellowButtGuy"], ["ShouSiJun", "ShouSiJun", "ShouSiJun"], ["BlackButtGuy", "BlackButtGuy", "BlackButtGuy", "BlackButtGuy"]]

let AdviceTitle: [[String]] = [["饭后对抗小肚腩", "饭后对抗小肚腩", "饭后对抗小肚腩"], [], [], []]
let AdviceBody: [[String]] = [["12 times", "10 times", "11 times"], [], [], []]


// Bluetooth Communication
let CushionServiceUUIDString = "FFE0"//"D0611E78-BBB4-4591-A5F8-487910AE4366" //"DEADF154-0000-0000-0000-0000DEADF154"
let CushionRequestCharacteristicUUIDString = "FFE1"//"8667556C-9A37-4C91-84ED-54EE27D90049" //"71DA3FD1-7E10-41C1-B16F-4430B506CDE7"
let CushionMotoDataCharacteristicUUIDString = "FFE1"//"71DA3FD1-7E10-41C1-B16F-4430B506CDF8"
let CheckValue = "D"

enum Request: Int {
    case withData = 1
    case withoutData = 0
}

enum MotorKeep: Int {
    case enable = 1
    case disable = 0
}

enum MotorSW: Int {
    case enable = 1
    case disable = 0
}

// Animation
let SportsGuyFiles = ["SportsGuy1-1", "SportsGuy1-2", "SportsGuy2-1", "SportsGuy2-2", "SportsGuy3-1", "SportsGuy3-2"]
