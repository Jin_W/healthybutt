//
//  Message.swift
//  HealthyButt
//
//  Created by Jin Wang on 9/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import Foundation
import UIKit

class Message {
    var icon: UIImage!
    var name: NSString?
    var metaIcon: NSString?
    var metaText: NSString?
    var body: NSString!
    
    init() {
        
    }
    
    class func initWithIcon(icon: UIImage, name: NSString?, metaIcon: NSString? ,metaText: NSString?, body: NSString) -> Message {
        let message = Message()
        message.icon = icon
        message.name = name
        message.metaIcon = metaIcon
        message.metaText = metaText
        message.body = body
        
        return message
    }
    
    class func generate() -> Message {
        return DataManager.generateMessage()
    }
    
    func save() -> NSInteger {
        return DataManager.addMessage(self)
    }
    
    class func findByIndex(index: NSInteger) -> Message {
        return DataManager.getMessageAtIndex(index)
    }
    
    func update() {
    
    }
    
    func delete() {
    
    }
    
}