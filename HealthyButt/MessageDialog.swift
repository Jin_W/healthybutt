//
//  MessageDialog.swift
//  HealthyButt
//
//  Created by Jin Wang on 8/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit


// Change it to different layout.
class MessageDialog: UIView {
    var portrait: UIImageView!
    var name: UILabel!
    var meta: UILabel!
    var body: UILabel!
    var control: UIButton!
    var background: UIImageView!
    var restSeconds: NSInteger = 0
    
    init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initWithPortrait(image: UIImage, withName: NSString?, withMetaIcon: NSString?, withMetaText: NSString?, withBody: NSString, forMinutes mins: NSInteger)
    {
        portrait = UIImageView(image: image)
        
        //        backgroundColor = UIColor(white: 1.0, alpha: 0.1)
        
        name = UILabel()
        
        if let _name = withName {
            name.text = _name
        }
        else {
            name.text = ""
        }
        
        meta = UILabel()
        
        var metaIcon = NSTextAttachment()
        var metaIconString: NSAttributedString
        var metaFull: NSMutableAttributedString
        
        if let _metaText = withMetaText {
            metaFull = NSMutableAttributedString(string: withMetaText)
        }
        else {
            metaFull = NSMutableAttributedString()
        }
        
        if let _metaIcon = withMetaIcon {
            metaIcon.image = UIImage(named: _metaIcon)
            metaIconString = NSAttributedString(attachment: metaIcon)
            metaFull.appendAttributedString(metaIconString)
        }
        
        meta.text = metaFull.string
        
        body = UILabel()
        
        body.text = withBody
        body.numberOfLines = 3
        
        control = UIButton()
        
        restSeconds = mins * 60
        control.setTitle(Utility.formatSeconds(restSeconds, alwaysWithHour: false), forState: UIControlState.Normal)

        
        addBackground()
        addPortrait()
        addName()
        addMeta()
        addBody()
        addControl()
        
//        countDown()
    }
    
    func initWithPortrait(image: UIImage, withBody: NSString) {
        
        backgroundColor = UIColor(white: 1.0, alpha: 1.0)
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        portrait = UIImageView(image: image)
        body = UILabel()
        body.text = withBody
        
        portrait.frame = CGRectMake(10, 10, 50, 50)
        body.frame = CGRectMake(70, 10, 200, 50)
        body.numberOfLines = 3
        
        body.font = UIFont(name: chineseFont, size: CGFloat(ReminderBodyTextSize))
        
        addSubview(portrait)
        addSubview(body)
    }
    
    func addBackground() {
        
        background = UIImageView(image: UIImage(named: "SocialBackground"))
        background.frame = CGRectMake(0, 0, frame.size.width, frame.size.height)
        
        addSubview(background)
    }
    
    func addPortrait() {
        portrait.frame = CGRectMake(20, 20, 30, 30)
        portrait.contentMode = UIViewContentMode.ScaleAspectFit
        
        addSubview(portrait)
    }
    
    func addName() {
        name.frame = CGRectMake(60, 50, 100, 20)
        name.center.y = portrait.center.y
        name.font = UIFont(name: chineseFont, size: CGFloat(MessageTitleSize))
        
        addSubview(name)
    }
    
    func addMeta() {
        meta.frame = CGRectMake(180, 20, 100, 20)
        meta.font = UIFont(name: chineseFont, size: CGFloat(MessageMetaTextSize))
        meta.sizeToFit()
        
        addSubview(meta)
    }
    
    func addBody() {
        body.frame = CGRectMake(20, 60, 260, 80)
        body.font = UIFont(name: chineseFont, size: CGFloat(MessageBodyTextSize))
        body.textAlignment = NSTextAlignment.Left
        
        addSubview(body)
    }
    
    func addControl() {
        control.frame = CGRectMake(20, 150, 260, 60)
        control.setImage(UIImage(named: "MessageDialogButtonBox"), forState: UIControlState.Normal)
        control.imageView.contentMode = UIViewContentMode.ScaleAspectFit
        control.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        control.titleEdgeInsets = UIEdgeInsetsMake(0, -control.imageView.image.size.width, 0, 0)
        
        control.userInteractionEnabled = false
        
        control.titleLabel.font = UIFont(name: englishFont, size: CGFloat(MessageButtonTextSize))
        control.titleLabel.textColor = UIColor.blackColor()
        
        addSubview(control)
    }
    

    
    func tick() {
        
        restSeconds -= 1
        if restSeconds >= MessageCancelTime {
            control.setTitle(Utility.formatSeconds(restSeconds, alwaysWithHour: false), forState: UIControlState.Normal)
        }
        else if restSeconds >= 0 {
            control.setTitle("取消发送：\(Utility.formatSeconds(restSeconds, alwaysWithHour: false))", forState: UIControlState.Normal)
            control.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            control.setImage(UIImage(named: "MessageDialogCancelBox"), forState: UIControlState.Normal)
            control.titleLabel.font = UIFont(name: chineseFont, size: CGFloat(MessageButtonTextSize))
            control.userInteractionEnabled = true
        }
        else {
            
        }
    }
    
    func cancelSend() {
        // Show the cancel message
        generateFinishSendingLayout(nil, text: "发送已取消!")
    }
    
    func sentFinished() {
        // Show the sent message
        generateFinishSendingLayout(portrait.image, text: "发送成功!")
        
    }
    
    func generateFinishSendingLayout(icon: UIImage?, text: NSString) {
        removeViews()
        
        addBackground()
        
        var sentFinishButton = UIButton(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        
        sentFinishButton.setTitle(text, forState: UIControlState.Normal)
        sentFinishButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        sentFinishButton.titleLabel.font = UIFont(name: chineseFont, size: CGFloat(MessageCompleteTextSize))
        
        if let _icon = icon {
            sentFinishButton.setImage(_icon, forState: UIControlState.Normal)
            sentFinishButton.titleEdgeInsets = UIEdgeInsetsMake(frame.size.height / 4, -_icon.size.width, 0, 0)
            sentFinishButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, frame.size.height / 4, -sentFinishButton.titleLabel.frame.size.width)
            sentFinishButton.imageView.center.x = sentFinishButton.center.x
        }
        
        sentFinishButton.userInteractionEnabled = false
        
        addSubview(sentFinishButton)
        
    }
    
    func removeViews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func dismiss() {
        
        UIView.animateWithDuration(0.3, animations: {
            self.alpha = 0.1
            self.transform = CGAffineTransformMakeScale(0.1, 0.1)
            }, completion: {
                (Bool finished) -> Void in
                self.removeFromSuperview()
            })
    }
}