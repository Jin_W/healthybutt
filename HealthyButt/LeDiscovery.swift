//
//  LeDiscovery.swift
//  HealthyButt
//
//  Created by Jin Wang on 11/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import CoreBluetooth

/****************************************************************************/
/*                          UI protocols                                    */
/****************************************************************************/
protocol LeDiscoveryDelegate {
    func discoveryDidRefresh()
    func didConnectedToPeripheral(peripheral: CBPeripheral)
    func didDisconnectedPeripehral(peripheral: CBPeripheral)
    func discoveryStatePoweredOn()
}



/****************************************************************************/
/*                          Discovery class                                 */
/****************************************************************************/
//@interface LeDiscovery : NSObject
//
//+ (id) sharedInstance;


/****************************************************************************/
/*                              UI controls                                 */
/****************************************************************************/
//@property (nonatomic, assign) id<LeDiscoveryDelegate>           discoveryDelegate;
//@property (nonatomic, assign) id<LeTemperatureAlarmProtocol>    peripheralDelegate;


/****************************************************************************/
/*                              Actions                                     */
/****************************************************************************/
//- (void) startScanningForUUIDString:(NSString *)uuidString;
//- (void) stopScanning;
//
//- (void) connectPeripheral:(CBPeripheral*)peripheral;
//- (void) disconnectPeripheral:(CBPeripheral*)peripheral;


/****************************************************************************/
/*                          Access to the devices                           */
/****************************************************************************/
//@property (retain, nonatomic) NSMutableArray    *foundPeripherals;
//@property (retain, nonatomic) NSMutableArray    *connectedServices; // Array of LeTemperatureAlarmService
//@end

var this: LeDiscovery!
var previousState: CBCentralManagerState = CBCentralManagerState.Unknown



class LeDiscovery: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager: CBCentralManager!
    var pendingInit: Bool!
    var foundPeripherals: NSMutableArray = []
    var connectedServices: NSMutableArray = []
    var discoveryDelegate: LeDiscoveryDelegate!

    // pragma mark -
    // pragma mark Init
/****************************************************************************/
/*                                  Init                                    */
/****************************************************************************/
    
    class func sharedInstance() -> LeDiscovery {
        
        if !this {
            this = LeDiscovery()
        }
        
        return this
    }
    
    init() {
        super.init()
        
        pendingInit = true
        centralManager = CBCentralManager(delegate: self, queue: dispatch_get_main_queue())
        
        foundPeripherals = NSMutableArray()
        connectedServices = NSMutableArray()
    }

//#pragma mark -
//#pragma mark Restoring
/****************************************************************************/
/*                              Settings                                    */
/****************************************************************************/
/* Reload from file. */
    
    func loadSavedDevices() -> Void {
        
        var userDefauls = NSUserDefaults.standardUserDefaults()
        
        var storedDevices = NSUserDefaults.standardUserDefaults().arrayForKey("StoredDevices") as NSArray?
        
        if storedDevices == nil || !storedDevices?.isKindOfClass(NSArray) {
            println("No stored array to load")
            return
        }
        
        for deviceUUIDString in storedDevices as NSArray {
            
            if !deviceUUIDString.isKindOfClass(NSString) {
                continue
            }
            
            var uuid = CBUUID.UUIDWithString(String(deviceUUIDString as NSString))
            
            if !uuid {
                continue
            }
            
            centralManager.retrievePeripheralsWithIdentifiers(NSArray(object: uuid))
        }
    }
    
    func addSavedDevice(uuid: CBUUID) -> Void {
        var storedDevices = NSUserDefaults.standardUserDefaults().arrayForKey("StoredDevices") as NSArray
        var newDevices: NSMutableArray = []
        var uuidString: String
        
        if !storedDevices.isKindOfClass(NSArray) {
            println("Can't find/create an array to store the uuid")
            return
        }
        
        newDevices = NSMutableArray(array: storedDevices)
        
        uuidString = uuid.UUIDString
        
        if uuidString != nil {
            newDevices.addObject(uuidString)
        }
        
        /* Store */
        NSUserDefaults.standardUserDefaults().setObject(newDevices, forKey: "StoredDevices")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    
    
    func removeSavedDevice(uuid: CBUUID) {
        var storedDevices = NSUserDefaults.standardUserDefaults().arrayForKey("StoredDevices") as NSArray
        var newDevices: NSMutableArray = []
        var uuidString: String
        
        if storedDevices.isKindOfClass(NSArray) {
            newDevices = NSMutableArray(array: storedDevices)
            
            uuidString = uuid.UUIDString
            
            if uuidString != nil {
                newDevices.removeObject(uuidString)
            }
            
            /* Store */
            NSUserDefaults.standardUserDefaults().setObject(newDevices, forKey: "StoredDevices")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
    }
    
    
    func centralManager(central: CBCentralManager!, didRetrieveConnectedPeripherals peripherals: [AnyObject]!) {
        
        /* Add to list. */
        for peripheral in peripherals {
            central.connectPeripheral(peripheral as CBPeripheral, options: nil)
        }
        
        discoveryDelegate.discoveryDidRefresh()
    }
    
    func centralManager(central: CBCentralManager!, didRetrievePeripherals peripherals: [AnyObject]!) {
        for peripheral in peripherals {
            central.connectPeripheral(peripheral as CBPeripheral, options: nil)
        }
        
        discoveryDelegate.discoveryDidRefresh()
    }
    
    


//#pragma mark -
//#pragma mark Discovery
/****************************************************************************/
/*                              Discovery                                   */
/****************************************************************************/
    
    func startScanningForUUIDString(uuidString: NSString?) -> Void {
        
        if uuidString == nil {
            println("Start to scan all peripherals")
            
            var options = NSDictionary(object: NSNumber.numberWithBool(false), forKey: CBCentralManagerScanOptionAllowDuplicatesKey)
            
            centralManager.scanForPeripheralsWithServices(nil, options: options)
        }
        else {
            println("Start to scan peripheral advertising service with uuid: \(uuidString)")
            
            var uuidArray = NSArray(object: CBUUID.UUIDWithString(uuidString))
            
            var options = NSDictionary(object: NSNumber.numberWithBool(false), forKey: CBCentralManagerScanOptionAllowDuplicatesKey)
            
            centralManager.scanForPeripheralsWithServices(uuidArray, options: options)
        }
        
    }
    
    
    func stopScanning() -> Void {
        centralManager.stopScan()
    }
    
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        
        println("Peripheral \(peripheral.name) with UUID \(peripheral.identifier.UUIDString) found")
        
        if !foundPeripherals.containsObject(peripheral) {
            foundPeripherals.addObject(peripheral)
            discoveryDelegate.discoveryDidRefresh()
        }
    }

//#pragma mark -
//#pragma mark Connection/Disconnection
/****************************************************************************/
/*                      Connection/Disconnection                            */
/****************************************************************************/
    
    func connectPeripheral(peripheral: CBPeripheral) -> Void {
        
        if peripheral.state == CBPeripheralState.Disconnected {
            centralManager.connectPeripheral(peripheral, options: nil)
        }
    }
    
    func disconnectPeripheral(peripheral: CBPeripheral) -> Void {
        
        centralManager.cancelPeripheralConnection(peripheral)
        
    }
    
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
//        var service: LeCushionService
//        
//        if !connectedServices.containsObject(service) {
//            connectedServices.addObject(service)
//        }
//
//        if foundPeripherals.containsObject(peripheral) {
//            foundPeripherals.removeObject(peripheral)
//        }
//        
//        peripheralDelegate.alarmServiceDidChangeStatus(service)
//        
//        discoveryDelegate.discoveryDidRefresh()
        
        printPeripheralInfo(peripheral)
        
        discoveryDelegate.didConnectedToPeripheral(peripheral)

    }
    
    func centralManager(central: CBCentralManager!, didFailToConnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
        
        println("Attempted connection to peripheral \(peripheral.name) failed: \(error.localizedDescription)")
        
    }
    
    
    func centralManager(central: CBCentralManager!, didDisconnectPeripheral peripheral: CBPeripheral!, error: NSError!) {
        
        println("Disconnected from peripheral: \(peripheral.name)")
        
        for service in connectedServices {
            if service.peripheral == peripheral {
                connectedServices.removeObject(service)
//                peripheralDelegate.alarmServiceDidChangeStatus(service as LeCushionService)
                break
            }
        }
        
        discoveryDelegate.didDisconnectedPeripehral(peripheral)
        discoveryDelegate.discoveryDidRefresh()
        
    }
    
    func clearDevices() -> Void {
        
        foundPeripherals.removeAllObjects()
        
        for service in connectedServices {
            service.reset()
        }
        
        connectedServices.removeAllObjects()
        
    }
    
    /*
    *  @method printPeripheralInfo:
    *
    *  @param peripheral Peripheral to print info of
    *
    *  @discussion printPeripheralInfo prints detailed info about peripheral
    *
    */
    func printPeripheralInfo(periperal: CBPeripheral) {
        // CFStringRef s = CFUUIDCreateString(NULL, (__bridge CFUUIDRef )peripheral.identifier);
        var s = CBUUID.UUIDWithString(periperal.identifier.UUIDString)
        
        println("------------------------------------")
        println("Peripheral Info :")
        println("UUID : \(s)")
        println("RSSI : \(periperal.RSSI)")
        println("Name : \(periperal.name)")
        println("isConnected : \(periperal.state == CBPeripheralState.Connected)")
        println("------------------------------------")
    }
    
    func centralManagerDidUpdateState(central: CBCentralManager!) {
        
        switch centralManager.state {
        case CBCentralManagerState.PoweredOff:
            clearDevices()
            discoveryDelegate.discoveryDidRefresh()
            /* Tell user to power ON BT for functionality, but not on first run - the Framework will alert in that instance. */
            
            break
        case CBCentralManagerState.Unauthorized:
            /* Tell user the app is not allowed. */
            break
        
        case CBCentralManagerState.Unknown:
            /* Bad news, let's wait for another event. */
            break
        
        case CBCentralManagerState.PoweredOn:
            pendingInit = false
            loadSavedDevices()
            
            // Changed from [centralManager retrieveConnectedPeripherals];
//            centralManager.retrieveConnectedPeripheralsWithServices(nil)
//            discoveryDelegate.discoveryDidRefresh()
            discoveryDelegate.discoveryStatePoweredOn()
            break
            
        case CBCentralManagerState.Resetting:
            clearDevices()
            discoveryDelegate.discoveryDidRefresh()
//            peripheralDelegate.alarmServiceDidReset()
            
            pendingInit = true
            break
        default:
            println("Manager State Info: \(centralManager.state)")
            break
        }
        
        
        previousState = centralManager.state
        
    }
}