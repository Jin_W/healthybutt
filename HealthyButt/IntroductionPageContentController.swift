//
//  IntroductionController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

class IntroductionPageContentController: UIViewController {
    @IBOutlet var name: UILabel
    @IBOutlet var buttGuy: UIImageView
    @IBOutlet var butt: UIImageView
    
    var pageIndex: NSInteger! = 0
    var titleName: NSString! = ""
    var buttGuyFile: NSString! = ""
    var buttFile: NSString! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = titleName
        name.font = UIFont(name: chineseFont, size: (CGFloat)(introductionPageTitleSize))
        name.textAlignment = NSTextAlignment.Center
        name.textColor = UIColor.whiteColor()
        
        buttGuy.image = UIImage(named: buttGuyFile)
        butt.image = UIImage(named: buttFile)
        
        buttGuy.contentMode = UIViewContentMode.ScaleAspectFit
        butt.contentMode = UIViewContentMode.ScaleAspectFit
        
        view.backgroundColor = UIColor.clearColor()
    }
    
}