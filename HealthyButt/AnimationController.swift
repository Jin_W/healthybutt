//
//  AnimationController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

let changePoint1 = 30 * 60
let changePoint2 = 45 * 60

class AnimationController: UIViewController
{
    @IBOutlet var buttGuy: UIImageView
    @IBOutlet var butt: UIImageView
    
    var imageArray: Array<UIImage> = []
    var animationStart = false
    
    /**
     * Display the animation under the timer. This can be a gif image or any
     * other complicated animation.
     */
    override func viewDidLoad()
    {
        view.backgroundColor = UIColor.clearColor()
        
//        var backgroundImage = Utility.scaleImage(UIImage(named: "YellowButtSportsGuy"), toSize: imageView.frame.size)
//        buttGuy.image = UIImage(named: pageButtSportGuyFiles[currentIndex])
        buttGuy.image = UIImage(named: "YellowButtSportsGuy1")
        
        // This is better solution
        buttGuy.contentMode = UIViewContentMode.ScaleAspectFit
        
        butt.image = UIImage(named: pageButtFiles[currentIndex])
        butt.contentMode = UIViewContentMode.ScaleAspectFit
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("counterChanged:"), name: TimerCounterNotification, object: nil)
        
        for imageName in SportsGuyFiles {
            imageArray.append(UIImage(named: imageName))
        }
    }
    
    func counterChanged(notification: NSNotification) {
        var counter = notification.userInfo.bridgeToObjectiveC().valueForKey("counter") as NSInteger
        
        if counter > changePoint2 && animationStart == false {
            startAnimation()

        }
        else if counter > changePoint1 {
            buttGuy.image = UIImage(named: "YellowButtSportsGuy2")
        }
        else if counter == 0 {
            reset()
        }
        else {
            
        }
    }
    
    func reset() {
        buttGuy.stopAnimating()
        buttGuy.image = UIImage(named: "YellowButtSportsGuy1")
        animationStart = false
    }
    
    func startAnimation() {
        buttGuy.animationImages = imageArray
        buttGuy.animationDuration = 3.0
        buttGuy.startAnimating()
        
        animationStart = true
    }
}
