//
//  SettingController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

/**
 * This is the setting page controller. Should be static table.
 */
class SettingController: UITableViewController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        
        var backButton = UIBarButtonItem()
        backButton.title = ""
        
        navigationController.navigationBar.topItem.backBarButtonItem = backButton
        
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController.navigationBar.tintColor = UIColor.blackColor()
        
        var backIndicator = Utility.scaleImage(UIImage(named: "BackIcon"), toSize: NaviBarIconSize)
        
        navigationController.navigationBar.backIndicatorImage = backIndicator
        navigationController.navigationBar.backIndicatorTransitionMaskImage = backIndicator
        
        
        navigationController.navigationBar.titleTextAttributes = NSDictionary(objects: [UIFont(name: chineseFont, size: 26), UIColor.blackColor()], forKeys: [NSFontAttributeName, NSForegroundColorAttributeName])
    }
}
