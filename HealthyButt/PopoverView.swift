//
//  PopoverView.swift
//  HealthyButt
//
//  Created by Jin Wang on 8/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

let kGradientTopColor = UIColor(white: 1.0, alpha: 0.95)
let kGradientBottomColor = UIColor(white: 0.98, alpha: 0.95)

class PopoverView: UIView {
    
    var contentView: UIView!
    var boxFrame: CGRect!
    
    init(frame: CGRect)  {
        super.init(frame: frame)
        // clear the default background color in order to be transparent
        self.backgroundColor = UIColor.clearColor()
    }
    
    class func showPopoverAtPoint(point: CGPoint, fromView view: UIView, inView topView: UIView, withContentView cView: UIView, dismissEnable: Bool) -> PopoverView {
        let popoverView = PopoverView(frame: CGRectZero)
        popoverView.showPopoverAtPoint(point, fromView: view, inView: topView, withContentView: cView, dismissEnable: dismissEnable)
        
        return popoverView
    }
    
    func showPopoverAtPoint(point: CGPoint, fromView view: UIView, inView topView: UIView, withContentView cView: UIView, dismissEnable: Bool) {
        
        boxFrame = cView.frame
        contentView = cView
        
        // Detail
        var window = UIApplication.sharedApplication().keyWindow
        if (!window) {
            window = UIApplication.sharedApplication().windows[0] as UIWindow
        }
        
        // topView could be DisplayController?
        var topView = topView
        var topPoint = topView.convertPoint(point, fromView: view)
        var topViewBounds = topView.bounds
        contentView.frame = boxFrame
        contentView.hidden = false
        addSubview(contentView)
        
        layer.anchorPoint = CGPointMake(topPoint.x / topViewBounds.size.width, topPoint.y / topViewBounds.size.height)
        frame = topView.frame
        setNeedsDisplay() // redraw
        
        topView.addSubview(self)
        
        // Add action
        if dismissEnable {
            var tap = UITapGestureRecognizer(target: self, action: "tapped:")
            addGestureRecognizer(tap)
            userInteractionEnabled = true
        }
        alpha = 0.0
        transform = CGAffineTransformMakeScale(0.1, 0.1)
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.alpha = 1.0
            self.transform = CGAffineTransformMakeScale(1.05, 1.05)
            }, completion: {
                (Bool finished) -> Void in
                UIView.animateWithDuration(0.08, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                    self.transform = CGAffineTransformIdentity
                    }, completion: nil)
            })
    }
    
//    override func drawRect(rect: CGRect) {
//        var frame = boxFrame
//        
//        var xMin = CGRectGetMinX(frame)
//        var yMin = CGRectGetMinY(frame)
//        var xMax = CGRectGetMaxX(frame)
//        var yMax = CGRectGetMaxY(frame)
//        var radius = 4.0
//        var cpOffset = 1.8
//        
//        var popoverPath = UIBezierPath()
//        
//        // Top-left
//        popoverPath.moveToPoint(CGPointMake(xMin, yMin + radius))
//        popoverPath.addCurveToPoint(CGPointMake(xMin + radius, yMin), controlPoint1: CGPointMake(xMin, yMin + radius - cpOffset), controlPoint2: CGPointMake(xMin + radius - cpOffset, yMin))
//        
//        // Top-right
//        popoverPath.addLineToPoint(CGPointMake(xMax - radius, yMin))
//        popoverPath.addCurveToPoint(CGPointMake(xMax, yMin + radius), controlPoint1: CGPointMake(xMax - radius + cpOffset, yMin), controlPoint2: CGPointMake(xMax, yMin + radius - cpOffset))
//        
//        // Bottom-right
//        popoverPath.addLineToPoint(CGPointMake(xMax, yMax - radius))
//        popoverPath.addCurveToPoint(CGPointMake(xMax - radius, yMax), controlPoint1: CGPointMake(xMax, yMax - radius + cpOffset), controlPoint2: CGPointMake(xMax - radius + cpOffset, yMax))
//
//        // Bottom-left
//        popoverPath.addLineToPoint(CGPointMake(xMin + radius, yMax))
//        popoverPath.addCurveToPoint(CGPointMake(xMin, yMax - radius), controlPoint1: CGPointMake(xMin + radius - cpOffset, yMax), controlPoint2: CGPointMake(xMin, yMax - radius + cpOffset))
//        
//        // Draw it
//        popoverPath.closePath()
//        
//        var colorSpace = CGColorSpaceCreateDeviceRGB()
//        var context = UIGraphicsGetCurrentContext()
//        var shadow = UIColor(white: 0.0, alpha: 0.4)
//        var shadowOffset = CGSizeMake(0, 1)
//        var shadowBlurRadius = 10.0
//        
//        var gradientColors = [kGradientTopColor.CGColor, kGradientBottomColor.CGColor, nil]
//        let colors: CFArray = [kGradientTopColor.CGColor, kGradientBottomColor.CGColor]
//        var gradientLocations = [0.0, 1.0]
//        var gradient = CGGradientCreateWithColors(colorSpace, colors, gradientLocations)
//        
//        var bottomOffset = 0.0
//        var topOffset = 0.0
//        
//        CGContextSaveGState(context)
//        CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor)
//        CGContextBeginTransparencyLayer(context, nil)
//        
//        popoverPath.addClip()
//        
//        // Don't draw the gradient, leave the back ground transparent.
////        CGContextDrawLinearGradient(context, gradient, CGPointMake(CGRectGetMidX(frame), CGRectGetMinY(frame) - topOffset), CGPointMake(CGRectGetMidX(frame), CGRectGetMaxY(frame) + bottomOffset), 0)
//        CGContextEndTransparencyLayer(context)
//        CGContextRestoreGState(context)
//        
//        CGGradientRelease(gradient)
//        CGColorSpaceRelease(colorSpace)
//    }
    
    func tapped(tap: UITapGestureRecognizer) {
        dismiss()
    }
    
    func dismiss() {
        UIView.animateWithDuration(0.3, animations: {
            self.alpha = 0.1
            self.transform = CGAffineTransformMakeScale(0.1, 0.1)
            }, completion: {
            (Bool finished) -> Void in
                self.removeFromSuperview()
            })
    }
}
