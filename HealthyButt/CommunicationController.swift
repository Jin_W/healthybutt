//
//  CommunicationController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit

import CoreBluetooth

var communicationTimer: NSTimer!
var vibrateRequest = 0
var communicationFlag = false

// This class will communicate with the cushion via CoreBluetooth which supports bluetooth 4.0.
class CommunicationController: UITableViewController, LeDiscoveryDelegate, CBPeripheralDelegate {
    var centralManager: LeDiscovery!
    var connectedPeripheral: CBPeripheral!
    var data: NSMutableData = NSMutableData()
    var connectedServices: NSMutableArray = []
    var cushionService: CBService!
    var cushionMotorDataCharacteristic: CBCharacteristic!
    var cushionRequestCharacteristic: CBCharacteristic!
    var cushionMotorData: NSDictionary!
    
    var devices: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centralManager = LeDiscovery.sharedInstance() as LeDiscovery
        centralManager.discoveryDelegate = self
        
        devices = centralManager.foundPeripherals
        
        tableView.backgroundColor = backgroundColor
        
        navigationItem.title = "选择坐垫"
        
    }
    
    
    func discoveryDidRefresh() {
        tableView.reloadData()
    }
    
    // This cannot call in the viewDidLoad. Or it won't start scanning...
    func discoveryStatePoweredOn() {
        centralManager.startScanningForUUIDString(nil)
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var cell = tableView.dequeueReusableCellWithIdentifier("deviceInfo", forIndexPath: indexPath) as UITableViewCell

        var peripheral: CBPeripheral

        var deviceName = cell.viewWithTag(1) as UILabel

        devices = LeDiscovery.sharedInstance().foundPeripherals
        
        if devices.count > indexPath.row {
            
            peripheral = devices.objectAtIndex(indexPath.row) as CBPeripheral
            
            deviceName.text = peripheral.name
        
        }
        else {
            deviceName.text = "Searching..."
        }

//        deviceName.text = "Click me to start!"
        deviceName.center = cell.center
        deviceName.textAlignment = NSTextAlignment.Center
        deviceName.textColor = UIColor.whiteColor()
        deviceName.font = UIFont(name: englishFont, size: CGFloat(MessageBodyTextSize))
        
        cell.backgroundColor = backgroundColor
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return devices.count
//        return 1
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        var peripheral: CBPeripheral
        var devices: NSArray
        
        LeDiscovery.sharedInstance().stopScanning()
        
        devices = LeDiscovery.sharedInstance().foundPeripherals
        peripheral = devices.objectAtIndex(indexPath.row) as CBPeripheral
        
        if peripheral.state == CBPeripheralState.Disconnected {
            
            LeDiscovery.sharedInstance().connectPeripheral(peripheral)
            
        }
//        communicationFlag = true
//        NSNotificationCenter.defaultCenter().postNotificationName(BluetoothConnectionStateChanged, object: self)
    }
    
    func findServiceFromPeripheral(serviceUUIDString: NSString, peripheral: CBPeripheral) -> CBService? {
        
        var targetUUID = CBUUID.UUIDWithString(serviceUUIDString)
        
        for service in peripheral.services {
            var uuid = service.UUID as CBUUID
            if uuid.isEqual(targetUUID) {
                return service as? CBService
            }
        }
        
        return nil
    }
    
    /* This function will be called when the LeDiscovery connected to the peripheral */
    func didConnectedToPeripheral(peripheral: CBPeripheral) {
        
        var serviceUUID = CBUUID.UUIDWithString(CushionServiceUUIDString)
        var serviceArray = NSArray(objects: serviceUUID) // NEED TO CONFIRM IF THE INIT SHOULD BE ENDED WITH nil
        
        peripheral.delegate = self
        
        connectedPeripheral = peripheral
        
        connectedPeripheral.discoverServices(serviceArray)
        
//        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func didDisconnectedPeripehral(peripheral: CBPeripheral) {
        stopDataCollection()
        
        if communicationFlag == true {
            communicationFlag = false
            
            NSNotificationCenter.defaultCenter().postNotificationName(BluetoothConnectionStateChanged, object: self)
        }
        
        // Should implement reconnection up to 3 times or 5 seconds.
//        centralManager.centralManager.retrievePeripheralsWithIdentifiers([connectedPeripheral.identifier])
    }
    
    /****************************************************************************/
    /*                          Service Interactions                            */
    /****************************************************************************/

    
    /*
    *  @method getAllCharacteristicsFromKeyfob
    *
    *  @param p Peripheral to scan
    *
    *
    *  @discussion getAllCharacteristicsFromKeyfob starts a characteristics discovery on a peripheral
    *  pointed to by p
    *
    */
    func getAllCharacteristicsFromPeripheral(peripheral: CBPeripheral) {
        for i in 0..<peripheral.services.count {
            var s = peripheral.services.bridgeToObjectiveC().objectAtIndex(i) as CBService
            println("Fetching characteristics for service with UUID : \(s.UUID.UUIDString)")
            
            peripheral.discoverCharacteristics(nil, forService: s)
        }
    }
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
        
        if error != nil {
            println("Error: \(error)")
        }
        else {
            var services = peripheral.services
            
            
            // ignore services is nil
            if services.count == 0 {
                println("No service found in this peripheral")
                
                var alert = UIAlertView(title: "Error", message: "Not the correct cushion.", delegate: self, cancelButtonTitle: "OK")
                alert.show()
                
                centralManager.disconnectPeripheral(peripheral)
            }
            else {
                
                if findServiceFromPeripheral(CushionServiceUUIDString, peripheral: peripheral) != nil {
                    println("The correct service in peripheral was found")
                }
                else {
                    var alert = UIAlertView(title: "Error", message: "This is not a request service available!", delegate: self, cancelButtonTitle: "OK")
                    alert.show()
                }
                
                for service in services {
                    var serviceUUID = service.UUID as CBUUID
                    
                    println("Service with UUID \(serviceUUID.UUIDString) found")
                    
                    if serviceUUID.isEqual(CBUUID.UUIDWithString(CushionServiceUUIDString)) {
                        cushionService = service as CBService
                        break
                    }
                }
            }
            
            if cushionService {
                println("Searching characteristics for service: \(cushionService)")
                var characteristicUUIDs = NSArray(objects: CushionRequestCharacteristicUUIDString, CushionMotoDataCharacteristicUUIDString)
                peripheral.discoverCharacteristics(nil, forService: cushionService)
            }
        }
    }
    
    
    func peripheral(peripheral: CBPeripheral!, didDiscoverCharacteristicsForService service: CBService!, error: NSError!) {
        
        var characteristics = service.characteristics
        
        if service != cushionService {
            println("Wrong Service")
        }
        else {
            if error != nil {
                println("Error: \(error.localizedDescription)")
            }
            else {
                if characteristics.count == 0 {
                    println("No characteristic for this service found")
                }
                else {
                    for characteristic in characteristics {
                        println("discovered characteristic \(characteristic.UUID)")
                        
                        var characteristicUUID = characteristic.UUID as CBUUID
                        
                        if characteristicUUID.isEqual(CBUUID.UUIDWithString(CushionRequestCharacteristicUUIDString)) {
                            println("Discovered Cushion Request Characteristic")
                            cushionRequestCharacteristic = characteristic as CBCharacteristic
                        }
                        
                        if characteristicUUID.isEqual(CBUUID.UUIDWithString(CushionMotoDataCharacteristicUUIDString)) {
                            println("Discovered Cushion Motor Data Characteristic")
                            cushionMotorDataCharacteristic = characteristic as CBCharacteristic
                        }
                        
                    }
                    
                    if cushionRequestCharacteristic != nil && cushionMotorDataCharacteristic != nil {
                        // enter the main page
//                        var displayController = storyboard.instantiateViewControllerWithIdentifier("DisplayController") as DisplayController
                        
//                        var sidebarController = storyboard.instantiateViewControllerWithIdentifier("SidebarViewController") as SidebarViewController
//                        
//                        var navigationController = storyboard.instantiateViewControllerWithIdentifier("NavigationController") as UINavigationController
//                        
//                        var revealViewController = SWRevealViewController(rearViewController: sidebarController, frontViewController: navigationController)
                        
                        //        window.rootViewController = revealViewController
                        
//                        presentViewController(revealViewController, animated: true, completion: nil)
                        
//                        navigationController.presentViewController(displayController, animated: true, completion: nil)
                        
                        communicationFlag = true
                        
                        NSNotificationCenter.defaultCenter().postNotificationName(BluetoothConnectionStateChanged, object: self)
                        
                        

                    }
                    else {
                        println("Something wrong")
                    }
                }
            }

        }
    }
    
    
    /****************************************************************************/
    /*                      Characteristics Interactions                        */
    /****************************************************************************/
    
    func writeCushionRequest(request: Request, motorKeep: MotorKeep, motorSW: MotorSW, motorTime: Int, motorVol: Array<UInt8>, check: String) {
        
        var data: NSData = NSData()
        
        if !cushionService {
            println("Not connected to the Cushion")
        }
        else {
            if !cushionRequestCharacteristic {
                println("Not valid Cushion Data Characteristic")
            }
            else {
                
                var dataDictionary: NSDictionary
                
                if request.hashValue == 1 {
                    dataDictionary = NSDictionary(objectsAndKeys: "\(request)", "T", check, "K")
                }
                else {
                    var motorVolDictionary = NSDictionary(objects: ["\(motorVol[0])", "\(motorVol[1])", "\(motorVol[2])", "\(motorVol[3])"], forKeys: ["A", "B", "C", "D"])
                    dataDictionary = NSDictionary(objectsAndKeys: "\(request)", "T", "\(motorKeep)", "P", "\(motorSW)", "W", "\(motorTime)", "E", motorVolDictionary, "L", check, "K")
                    
                }
                
                var dataJsonString = NSString(data: toJSONData(dataDictionary), encoding: NSUTF8StringEncoding)
                
                println("dataDictionary: \(dataDictionary)")
                println("toJSONData(dataDictionary): \(toJSONData(dataDictionary))")
                println("dataJsonString: \(dataJsonString)")
                
                connectedPeripheral.writeValue(toJSONData(dataDictionary), forCharacteristic: cushionRequestCharacteristic, type: CBCharacteristicWriteType.WithResponse)
            }
        }
    }
    
    

    func peripheral(peripheral: CBPeripheral!, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        if characteristic != cushionMotorDataCharacteristic || peripheral != connectedPeripheral {
            println("Peripheral error or characteristic error")
        }
        else {
            if error != nil {
                println("Read Value Error: \(error.localizedDescription)")
            }
            else {
                var motorDataJson = characteristic.value
                
                println("Read motor data: \(motorDataJson)")
                
                cushionMotorData = parseJSON(motorDataJson)
                
                if cushionMotorData["CHECK"] as NSString != CheckValue {
                    
                }
                else {
                    // Check the data and find way to update the view Controller.
                    PoseController.poseStateUpdate(cushionMotorData["VOL"] as NSDictionary)
                    
                    // Unsubscribe until the next request?
                    peripheral.setNotifyValue(false, forCharacteristic: cushionMotorDataCharacteristic)
                }
            }
        }
    }

    func peripheral(peripheral: CBPeripheral!, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        
        if characteristic != cushionMotorDataCharacteristic || peripheral != connectedPeripheral {
            println("Peripheral error or characteristic error")
        }
        else {
            if error != nil {
                println("Updata Notification Error: \(error.localizedDescription)")
            }
            else {
                
                println("Get Notification for Motor Data")
                
                peripheral.readValueForCharacteristic(cushionMotorDataCharacteristic)
            }
        }
    }
    
    
    func peripheral(peripheral: CBPeripheral!, didWriteValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        
        if characteristic != cushionRequestCharacteristic || peripheral != connectedPeripheral {
            println("Peripheral error or characteristic error")
        }
        else {
            if error != nil {
                println("Write Error: \(error.localizedDescription)")
            }
            else {
                
                println("Write Request Successfully")
                
                // subscribe the change of the motor data
                peripheral.setNotifyValue(true, forCharacteristic: cushionMotorDataCharacteristic)
            }
        }
    }
    
    func toJSONData(theData: NSDictionary) -> NSData? {
        
        var error: AutoreleasingUnsafePointer<NSError?> = nil
        
        var jsonData = NSJSONSerialization.dataWithJSONObject(theData, options: NSJSONWritingOptions.PrettyPrinted, error: error) as NSData
        
        if jsonData.length > 0 && error == nil {
            return jsonData
        }
        else {
            return nil
        }
        
    }
    
    func parseJSON(inputData: NSData) -> NSDictionary {
        var error: NSError?
        var boardsDictionary = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as NSDictionary
        
        return boardsDictionary
    }
    
//    class func poseDetector() -> NSInteger {
//        
//        if drand48() < 0.0003 {
//            return 1 // User Up
//        }
//        else {
//            return 0
//        }
//    }
//    
//    func alert()
//    {
//        
//    }
    
    func startDataCollection(interval: NSTimeInterval) {
        communicationTimer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: "dataCollection", userInfo: nil, repeats: true)
    }
    
    func dataCollection() {
        
        if vibrateRequest == 1 {
            writeCushionRequest(Request.withoutData, motorKeep: MotorKeep.disable, motorSW: MotorSW.enable, motorTime: 5, motorVol: [UInt8(200), UInt8(200), UInt8(200), UInt8(200)], check: CheckValue)
            vibrateRequest = 0
        }
        else {
            writeCushionRequest(Request.withData, motorKeep: MotorKeep.disable, motorSW: MotorSW.disable, motorTime: 0, motorVol: [UInt8(0), UInt8(0), UInt8(0), UInt8(0)], check: CheckValue)
        }

    }
    
    func stopDataCollection() {
        
        if communicationTimer {
            communicationTimer.invalidate()
        }
        
        
    }
    
}