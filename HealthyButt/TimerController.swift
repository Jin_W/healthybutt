//
//  TimerController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit
import Foundation

class TimerController: UIViewController, UIGestureRecognizerDelegate
{
    var timeDots: Array<UIImageView> = []
//    var cellCount: NSInteger!
    var center: CGPoint!
    var timer: NSTimer!
    var timerLable = UILabel()
    var counter = 0
    var reminderPointCounter = 0
    var timerControllerDelegate: TimerControllerDelegate?
    let startChangeTime = 3600 / dotNumber
//    var iconsWithMessages: NSMutableDictionary = NSMutableDictionary()
    var reminderIcons: Array<UIImageView> = []
    var messageIcons: Array<UIImageView> = []
    
    var currentTurnWrongPoseTime = 0
    
    override func viewDidLoad() {
        
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        // Initialize the timer
        setupTimer()
        
        // Start the timer
//        start()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("connectionStateChanged"), name: BluetoothConnectionStateChanged, object: nil)
        
//        ready()
    }
    
    func connectionStateChanged() {
        if communicationFlag == false {
            if timer {
                timer.invalidate()
            }
//            navigationController.pushViewController(communicationController, animated: true)
        }
        else {
            restart()
        }
    }
    
    /**
     * Set up the timer, used for setting the critical points during the 
     * sitting time.
     */
    func setupTimer() {
        setupDots()
        setupInfoIcons()
        
        timerLable.frame.size = CGSizeMake(CGFloat(radius) * sqrt(2), CGFloat(radius) * sqrt(2))
        timerLable.center = center
        timerLable.numberOfLines = 1
        timerLable.adjustsFontSizeToFitWidth = true
        timerLable.text = "Ready..."
        timerLable.textAlignment = NSTextAlignment.Center
        timerLable.textColor = UIColor.whiteColor()
        timerLable.font = UIFont.systemFontOfSize(CGFloat(timerLabelSize))
        
        view.addSubview(timerLable)
    }
    
    func reset() {
        timerLable.text = "Ready..."
        counter = 0
        reminderPointCounter = 0
        for i in 0..<dotNumber {
            timeDots[i].image = UIImage(named: "GreenDot")
        }
        for i in 0..<reminderIcons.count {
            reminderIcons[i].image = UIImage()
        }
        for i in 0..<messageIcons.count {
            messageIcons[i].image = UIImage()
            messageIcons[i].userInteractionEnabled = false
        }
    }
    
    func setupDots() {
        prepareLayout()
        for i in 0..<dotNumber {
            timeDots.append(UIImageView(image: UIImage(named: "GreenDot")))
            layoutDot(i)
            view.addSubview(timeDots[i])
        }
    }
    
    func setupInfoIcons() {
        
        for i in 0..<(reminderPoints.count + 1) {
            reminderIcons.append(UIImageView())
            layoutReminderIcon(i)
            view.addSubview(reminderIcons[i])
        }
        
        for i in 0..<(reminderPoints.count) {
            messageIcons.append(UIImageView())
            layoutMessageIcon(i)
            view.addSubview(messageIcons[i])
        }
    }
    
    func ready() {
        communicationController.startDataCollection(1.0)
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("readyCheck"), userInfo: nil, repeats: true)
    }
    
    func readyCheck() {
        if poseState == -1 {
            
        }
        else {
//            println("Get the cushion data: \(cushionData)")
            
            UIAlertView(title: "Get cushion data", message: "\(cushionData)", delegate: self, cancelButtonTitle: "Start").show()
            
            communicationController.stopDataCollection()
            timer.invalidate()
            start()
        }
    }
    
    func start() {
        
        communicationController.startDataCollection(dataCollectionTimeInterval)
        
        timer = NSTimer.scheduledTimerWithTimeInterval((1.0 / timerSpeed), target: self, selector: Selector("tick"), userInfo: nil, repeats: true)
    }
    
    func pause() {
        
    }
    
    func stop() {
        
    }
    
    /**
     * Update the time. Should be call every seconds. NOTE: There is no maximum hour currently, should be discussed
     * later.
     */
    func tick() -> Void {
//        counter += (Int)(1 * timerSpeed)
        
        if communicationFlag == false {
            timer.invalidate()
            
            return
        }
        
        
        if counter % Int(dataCollectionTimeInterval) == 0 {
            if poseState == -1 {
                userLeaveCushion()
                return
            }
            else if poseState == 0 {
                currentTurnWrongPoseTime += 1
            }
            else if poseState == 1 {
            }
            else {
                
            }
        }

        
//         Uncomment the following snippet if want to test without communication controller 
//        if PoseController.randomPose() == -1 {
//            userLeaveCushion()
//            return
//        }
        
        // If hour is expected to alway display, change withHour to ture.
        timerLable.text = Utility.formatSeconds(counter, alwaysWithHour: false)
        
        // Update the dot color if necessary
        
        // Currently, changing the speed may effect the activation of the critical points. And the dot must be updated one
        // by one. The color of the dot will be changed to white the first hour. Then will be changed to yellow if overTime.
        // TODO: This part should be rewritten to fully support different dotNumber and overTime.!!!
        if counter - startChangeTime >= 0 && (counter - startChangeTime) % (3600 / dotNumber) == 0 {
            var targetDotIndex = (Int)(counter / (3600 / dotNumber)) - 1
            
            if targetDotIndex >= 0 {
                if targetDotIndex < dotNumber {
                    timeDots[targetDotIndex].image = UIImage(named: "WhiteDot")
                    
                    // Check if the time reaches the critical points, currently in some cases, this check will never true.
                    // thus the icon cannot be put.
                    if counter == overTime % 3600 + 3600 / dotNumber {
                        // Set the overTime reminder
                        reminderIcons[0].image = UIImage(named: "RemindWhiteIcon")
                    }
                    else if reminderPointCounter >= reminderPoints.count {
                        
                    }
                    else if counter == (overTime + reminderPoints[reminderPointCounter]) % 3600 + 3600 / dotNumber {
                        reminderIcons[reminderPointCounter + 1].image = UIImage(named: "RemindWhiteIcon")
                    }
                    else if counter == (overTime + reminderPoints[reminderPointCounter] + reminderTime * 60) % 3600 + 3600 / dotNumber {
                        messageIcons[reminderPointCounter].image = UIImage(named: "SocialWhiteIcon")
                        reminderPointCounter += 1
                    }
                    else {
                        
                    }
                }
                else if counter < overTime + 3600 / dotNumber {
                    
                }
                else {
                    timeDots[targetDotIndex % dotNumber].image = UIImage(named: "YellowDot")
                    if counter == overTime + 3600 / dotNumber {
                        reminderIcons[0].image = UIImage(named: "RemindYellowIcon")
                        reminderPointCounter = 0
                        
                        // Vibrate the cushion
                        vibrateRequest = 1
                    }
                    else if reminderPointCounter >= reminderPoints.count {
                        
                    }
                    else if counter == overTime + reminderPoints[reminderPointCounter] + 3600 / dotNumber {
                        // Some actions should be taken here.
                        reminderIcons[reminderPointCounter + 1].image = UIImage(named: "RemindYellowIcon")
                        
                        // Vibrate the cushion
                        vibrateRequest = 1
                        
                        alert()
                        
//                        reminderPointCounter += 1
                    }
                    else {
                        
                    }
                }
                
                if currentTurnWrongPoseTime > 0 {
                    timeDots[targetDotIndex % dotNumber].image = UIImage(named: "RedDot")
                }
                currentTurnWrongPoseTime = 0

//                if PoseController.randomPoseWithoutCommunication() == 0 {
//                    timeDots[targetDotIndex % dotNumber].image = UIImage(named: "RedDot")
//                }
            }
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName(TimerCounterNotification, object: self, userInfo: NSDictionary(object: counter, forKey: "counter"))
        
        counter += 1
    }
    
    func changeDot(index: NSInteger, withImage image: UIImage) {
        timeDots[index].image = image
    }
    
    // Not usable now.
    func resetTimerLocation() {
        timerLable.sizeToFit()
        timerLable.center = center
    }
    
    /**
     * Call this method when the time is over a certain point.
     */
    func alert() {
        
        // Show Message Box
        timerControllerDelegate?.overtime?(self)
    }
    
    /**
     * Call this method when the user leave the seat.
     */
    func restart() {
        reset()
//        start()
        ready()
    }
    
    func prepareLayout() {
//        cellCount = 0
        center = CGPointMake(CGFloat(view.bounds.size.width / 2.0), CGFloat(timerHeight / 2.0))
    }
    
    func layoutDot(index: NSInteger) {
        var angle = Float(2.0) * Float(M_PI) * Float(index) / Float(dotNumber)
        var x = Float(center.x) + Float(radius) * sin(angle)
        var y = Float(center.y) - Float(radius) * cos(angle)
        
        timeDots[index].frame.size = CGSizeMake(CGFloat(dotSize), CGFloat(dotSize))
        timeDots[index].center = CGPointMake(CGFloat(x), CGFloat(y))
    }
    
    // Separate the reminders, dots and messages.
    func layoutReminderIcon(index: NSInteger) {
        var angle: Float
        if index == 0 {
            angle = Float(2.0) * Float(M_PI) * (Float(overTime) / 60 % 60) / Float(60)
        }
        else {
            angle = Float(2.0) * Float(M_PI) * ((Float(overTime) + Float(reminderPoints[index - 1])) / 60 % 60) / Float(60)
        }
        var x = Float(center.x) + (Float(radius) + sqrt(2) * Float(dotSize)) * Float(sin(angle))
        var y = Float(center.y) - (Float(radius) + sqrt(2) * Float(dotSize)) * Float(cos(angle))
        
        reminderIcons[index].frame.size = CGSizeMake(CGFloat(dotSize), CGFloat(dotSize))
        reminderIcons[index].center = CGPointMake(CGFloat(x), CGFloat(y))
    }
    
    func bindReminderIconWithMessage(relatedID: NSInteger?) {
        // Update reminder and message icons
        messageIcons[reminderPointCounter].image = UIImage(named: "SocialYellowIcon")
        
        // Store the corresponding message
        messageIcons[reminderPointCounter].tag = relatedID!
        
        // Add actions throught delegate
        
        var singleTap = UITapGestureRecognizer(target: self, action: "viewTapped:")
        singleTap.numberOfTapsRequired = 1
        messageIcons[reminderPointCounter].userInteractionEnabled = true
        messageIcons[reminderPointCounter].addGestureRecognizer(singleTap)
        
        // Increase index
        reminderPointCounter += 1
    }
    
    func viewTapped(recognizer: UITapGestureRecognizer) {
        var tapLocation = recognizer.locationInView(view)
        
        var fingerRect = CGRectMake(tapLocation.x - 5, tapLocation.y - 5, 10, 10);
        
        for subview in view.subviews {
            var subviewFrame = subview.frame
            if CGRectIntersectsRect(fingerRect, subviewFrame) && subview.isKindOfClass(UIImageView) {
                if subview.tag >= 0 {
                    reminderTapped(subview)
                }
            }
        }
    }
    
    func reminderTapped(sender: AnyObject) {
        var messageIcon = sender as UIImageView
        
        println(messageIcon.tag)
        
        timerControllerDelegate?.showMessage!(messageIcon.tag, AtPoint: messageIcon.center)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer!, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer!) -> Bool {
        return true
    }
    
    func reminderCanceled() {
        reminderPointCounter += 1
    }
    
    func layoutMessageIcon(index: NSInteger) {
        var angle: Float = Float(2.0) * Float(M_PI) * Float((overTime + reminderPoints[index] + reminderTime * 60) / 60 % 60) / 60
        var x = Float(center.x) + (radius + sqrt(2) * Float(dotSize)) * Float(sin(angle))
        var y = Float(center.y) - (radius + sqrt(2) * Float(dotSize)) * Float(cos(angle))

        messageIcons[index].frame.size = CGSizeMake(CGFloat(dotSize), CGFloat(dotSize))
        messageIcons[index].center = CGPointMake(CGFloat(x), CGFloat(y))
    }
    
    func userLeaveCushion() {
        
        timer.invalidate()
        
        var displayController = parentViewController as DisplayController
        
        displayController.presentAdviceViewController()
    }
    
    
}




/**
 * Delegation for Timer. Should be able to talk to MessageController,
 * AnamationController and ScoreController.
 */
@objc protocol TimerControllerDelegate {
    
    // Will be called after time over according to the critical time set at the beginning.
    @optional func overtime(target: UIViewController)
    
    // Will be called after the timer restarted.
    @optional func timerDidRestart()
    
    @optional func showMessage(messageID: NSInteger, AtPoint point: CGPoint)
    
    // Will be called before the timer restarted.
    @optional func timerWillRestart()
    
    
    
}
