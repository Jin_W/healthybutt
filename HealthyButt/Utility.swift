//
//  Utility.swift
//  HealthyButt
//
//  Created by Jin Wang on 8/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    class func getUIColorWithoutAlpha(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: 1.0)
    }
    
    func convertTimeToSeconds(time: NSString) -> Int {
        var t_arr = time.componentsSeparatedByString(":") as [String]
        var hh = t_arr[0]
        var mm = t_arr[1]
        var ss = t_arr[2]
        var s_now = hh.toInt()! * 3600 + mm.toInt()! * 60 + ss.toInt()!;
        return s_now
    }
    
    class func formatSeconds(value: Int, alwaysWithHour: Bool) -> String {
        var theTime = Int(value)
        var theTime1 = 0
        var theTime2 = 0
        var result = ""
        
        if theTime >= 60 {
            theTime1 = Int(theTime / 60)
            theTime = Int(theTime % 60)
            
            if theTime1 >= 60 {
                theTime2 = Int(theTime1 / 60)
                theTime1 = Int(theTime1 % 60)
            }
        }
        
        if theTime < 10 {
            result = "0\(Int(theTime))"
        }
        else {
            result = "\(theTime)"
        }
        
        if theTime1 >= 0 {
            if theTime1 < 10 {
                result = "0\(Int(theTime1))" + ":" + result
            }
            else {
                result = "\(Int(theTime1))" + ":" + result
            }
        }
        
        if theTime2 >= 0 {
            if theTime2 == 0 && !alwaysWithHour {
                
            }
            else if theTime2 < 10 {
                result = "0\(Int(theTime2))" + ":" + result
            }
            else {
                result = "\(Int(theTime2))" + ":" + result
            }
        }
        
        return result
    }
    
    class func scaleImage(image: UIImage, toSize newSize: CGSize) -> UIImage {
        var width = newSize.width
        var height = newSize.height
    
        UIGraphicsBeginImageContext(newSize)
        var rect = CGRectMake(0, 0, width, height)
        
        var widthRatio = image.size.width / width
        var heightRatio = image.size.height / height
        var divisor = widthRatio > heightRatio ? widthRatio : heightRatio
        
        width = image.size.width / divisor
        height = image.size.height / divisor
        
        rect.size.width = width
        rect.size.height = height
        
        //indent in case of width or height difference
        var offset = (width - height) / 2
        if offset > 0 {
            rect.origin.y = offset
        }
        else {
            rect.origin.x = -offset
        }
        
        image.drawInRect(rect)
        
        var newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    class func UIColorFromHex(hexValue: Int) -> UIColor {
        var red = ((CGFloat)((hexValue & 0xFF0000) >> 16)) / 255.0
        var green = ((CGFloat)((hexValue & 0xFF00) >> 8)) / 255.0
        var blue = (CGFloat)((hexValue & 0xFF)) / 255.0
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    class func printAvailableFonts() {
        for family in UIFont.familyNames() {
            println(family)
            
            for name in UIFont.fontNamesForFamilyName("\(family)") {
                println("   \(name)")
            }
        }
    }
    
}