//
//  PoseController.swift
//  HealthyButt
//
//  Created by Jin Wang on 9/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import Foundation

var poseState = -1
var cushionData: NSDictionary = NSDictionary()

class PoseController {
    
    // 协议接收部分，坐垫接收蓝牙串口传入数据(并且解析出马达震动方式、时间、强度)
    //格式如下
    //{"REQUEST":"0","MOTOR_KEEP":"0","MOTOR_SW":"1","MOTOR_TIME":"1000","MOTOR_VOL":{"A":"50","B":"100","C":"150","D":"200"},"CHECK":"THU"}
    //协议解析：
    //REQUEST为请求获取坐垫数据的作用，开启/关闭，取值范围：0/1
    //MOTOR_KEEP为震动是否在接收到一个指令周期后持续开启/关闭，取值范围：0/1
    //MOTOR_SW为震动使能作用，开启/关闭，取值范围：0/1
    //MOTOR_TIME为一个震动周期持续时间，吃在MOTOR_KEEP开启时，此函数将无效，取值范围：0~3600（不推荐太久）
    //MOTOR_VOL为四个震动马达的震动强度，取值范围：0~255
    //CHECK为校验部分，以验证Json命令的完整性，取值：“THU”
    
    //协议发送部分，坐垫向通过串口向蓝牙发送数据(坐垫收到请求命令时，采集几个传感器数值，并且发送出去)
    //参照接收部分的协议解析，最简单的是发送以下内容即可请求数据：{"REQUEST":"1","CHECK":"THU"}
    //格式如下
    //{"ID":"19920929","RSSI":"1","VOL":{"a":"250","b":"250","c":"150","d":"150","e":"150","f":"150"},"CHECK":"THU"}
    //协议解析：
    //ID为坐垫唯一ID，数字字母均可，不推荐太长
    //RSSI为蓝牙信号强度，暂无实际作用
    //VOL为六个应变片传感器的模拟值，取值范围：0~1023
    //CHECK为校验部分，以验证Json命令的完整性，取值：数字字母均可，不推荐太长
    //返回值大于1000表示没人，小于100是有人，有个数大于100小于1000表示姿势不对
    
    
    // {"a":"250","b":"250","c":"150","d":"150","e":"150","f":"150"}
    // return: -1: no seat, 0: pose incorrect, 1: pose correct
    class func poseStateUpdate(dataDictionary: NSDictionary) {
        
        var enumerator = dataDictionary.keyEnumerator()
        var key: NSString
        var motorData: Array<Int> = []
        cushionData = dataDictionary
        
        key = enumerator.nextObject() as NSString
        
        while key != nil {
            
            motorData.append(dataDictionary[key] as NSInteger)
            
            key = enumerator.nextObject() as NSString
        }

        if isSit(motorData) {
            
            if isSitRight(motorData) {
                poseState = 1
            }
            else {
                poseState = 0
            }
            
        }
        else {
            poseState = -1
        }
        
        
//        if drand48() > 0.9 {
//            
//        }
        
    }
    
    class func randomPose() -> Int {
        if drand48() > 0.9999 {
            return -1
        }
        else if drand48() > 0.9 {
            return 0
        }
        else {
            return 1
        }
    }
    
    class func randomPoseWithoutCommunication() -> Int {
        if drand48() > 0.999 {
            return 0
        }
        else {
            return 1
        }
    }
    
    
    class func isSit(data: Array<Int>) -> Bool {
        if averageOfIntArray(data) < 150 {
            return true
        }
        return false
    }
    
    class func isSitRight(data: Array<Int>) -> Bool {
        
        var rightVariance = varianceOfIntArray([data[1], data[2]])
        var leftVariance = varianceOfIntArray([data[5], data[6]])
        
        var diff = rightVariance > leftVariance ? rightVariance - leftVariance : leftVariance - rightVariance
        
        if diff > 30 {
            return false
        }
        
        return true
    }
    
    class func averageOfIntArray(intArray: Array<Int>) -> Float {
        
        var sum = 0
        
        for sub in intArray {
            sum += sub
        }
        
        return Float(sum) / Float(intArray.count)
    }
    
    class func varianceOfIntArray(intArray: Array<Int>) -> Float {
        
        var ave = averageOfIntArray(intArray)
        
        var sum: Float = 0.0
        
        for sub in intArray {
        
            var minus = Float(sub) > ave ? Float(sub) - ave : ave - Float(sub)
            
            sum += pow(minus, 2.0)
        }
        
        return sqrt(sum / Float(intArray.count))
        
    }
    
}