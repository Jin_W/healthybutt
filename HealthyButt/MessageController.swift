//
//  MessageController.swift
//  HealthyButt
//
//  Created by Jin Wang on 7/07/2014.
//  Copyright (c) 2014 UThoft. All rights reserved.
//

import UIKit
import QuartzCore

var message: Message!
var target: TimerController!
var timer: NSTimer!
var messageDialog: MessageDialog!
var popoverView: PopoverView?
var point: CGPoint!

class MessageController: UIViewController, TimerControllerDelegate {
    
    func overtime(_target: TimerController) {
        
        target = _target
        showMessage()
        
        countDown()
    }
    
    func countDown() {
        timer = NSTimer.scheduledTimerWithTimeInterval((1.0 / timerSpeed), target: self, selector: Selector("tick"), userInfo: nil, repeats: true)
    }
    
    func tick() {
        messageDialog.tick()
        messageDialog.control.addTarget(self, action: Selector("cancelSend"), forControlEvents: UIControlEvents.TouchUpInside)
        
        if messageDialog.restSeconds < 0 {
            sentFinished()
        }
    }
    
    func cancelSend() {
        timer.invalidate()
        messageDialog.cancelSend()
        
        timer = NSTimer.scheduledTimerWithTimeInterval((500.0 / timerSpeed), target: self, selector: Selector("dismissMessageDialog"), userInfo: nil, repeats: true)
        
        target.reminderCanceled()
    }
    
    func sentFinished() {
        timer.invalidate()
        messageDialog.sentFinished()
        
        timer = NSTimer.scheduledTimerWithTimeInterval((500.0 / timerSpeed), target: self, selector: Selector("dismissMessageDialog"), userInfo: nil, repeats: true)
        
        target.bindReminderIconWithMessage(message.save())
    }
    
    func dismissMessageDialog() {
        timer.invalidate()
        popoverView?.dismiss()
//        messageDialog.dismiss()
    }
    
    func newMessageDialogWithFrame() -> UIView {
        
        message = Message.generate()

        // The frame needed to be taken out or translated from the frame parameter.
        messageDialog = MessageDialog(frame: CGRectMake(0, 90, 300, 250))
        messageDialog.center.x = target.view.center.x
        messageDialog.initWithPortrait(message.icon, withName: message.name, withMetaIcon: message.metaIcon, withMetaText: message.metaText, withBody: message.body, forMinutes: reminderTime)
        
        return messageDialog
        
    }
    
    func shortMessageDialogWithMessage(_message: Message) -> UIView {
        
        var shortMessageDialog = MessageDialog(frame: CGRectMake(10, point.y - 20, 280, 70))
        shortMessageDialog.initWithPortrait(_message.icon, withBody: _message.body)
        
        return shortMessageDialog
    }
    
    func showMessage() {
        clearDialog()
        
        popoverView = PopoverView.showPopoverAtPoint(CGPointMake(target.view.frame.midX, target.view.frame.maxY), fromView: target.view, inView: target.parentViewController.view, withContentView: newMessageDialogWithFrame(), dismissEnable: false)
    }
    
    func showMessage(messageID: NSInteger, AtPoint _point: CGPoint) {
        point = _point
        clearDialog()
        
        popoverView = PopoverView.showPopoverAtPoint(point, fromView: target.view, inView: target.parentViewController.view, withContentView: shortMessageDialogWithMessage(Message.findByIndex(messageID)), dismissEnable: true)
    }
    
    func clearDialog() {
        popoverView?.dismiss()
    }
}